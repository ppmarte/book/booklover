<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "libros".
 *
 * @property int $id
 * @property string|null $ISBN
 * @property string|null $titulo
 * @property string|null $sipnosis
 * @property int|null $num_pag
 * @property string|null $f_publi
 * @property int|null $cod_lector
 * @property string|null $f_lectura
 * @property float|null $calificacion
 *
 * @property Agregar[] $agregars
 * @property Escritores[] $codEscritors
 * @property Estanterias[] $codEstanterias
 * @property Users $codLector
 * @property Comentarios[] $comentarios
 * @property Escribir[] $escribirs
 * @property Generos[] $generos
 */
class Libros extends \yii\db\ActiveRecord
{
   public $cod_escritor;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'libros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            
            [['num_pag', 'paginas_leidas','cod_lector'], 'integer'],
            [['calificacion'], 'number'],
           [['sipnosis', 'ISBN', 'titulo'],'required', 'message' => 'Campo requerido'],
            [['sipnosis'], 'string'],
            [['f_publi', 'f_lectura'], 'default', 'value' => date("Y-m-d")],
            [['ISBN'], 'string', 'max' => 20],
            [['ISBN'], 'match', 'pattern' => "/^([0-9a-z]+)$/i"],
            [['titulo'],'string',  'max' => 100],
            [['titulo'],  'match', 'pattern' => "/^([a-z\sáéíóúñÁÉÍÓÚüÜàèìòùÀÈÌÒÙÑ.]+)$/i"],
            [['cod_lector'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['cod_lector' => 'id']],
        ];
    }

    
     /*public function titulo_existe()
    {
  
  //Buscar el titulo en la tabla
  $table = Libros::find()->where("titulo=:titulo", [":titulo" => $this->titulo]);
  
  //Si el titulo existe mostrar el error
  if ($table->count() == 1)
  {
                $this->addError($this->titulo, "El librio ya existe");
  }
    }
    
    
     public function isbn_existe()
    {
  
  //Buscar el titulo en la tabla
  $table = Libros::find()->where("ISBN=:ISBN", [":ISBN" => $this->ISBN]);
  
  //Si el titulo existe mostrar el error
  if ($table->count() == 1)
  {
                $this->addError( $this->ISBN, "El ISBN ya existe");
  }
    }*/
    
    
    public function porcentaje_leido($paginas_leidas) {
        return $paginas_leidas / $this->num_pag * 100;
    }
    
    
    
public function actionCambiestantria($id)
{
    $libro = Libros::findOne($id);
    if ($libro->paginas_leidas == $libro->num_pag) {

        // Recuperar la instancia de Agregar correspondiente al libro
        $agregar = Agregar::find()->where(['cod_libro' => $libro->id])->one();

        // Actualizar el atributo cod_estanteria
        $estanteria = Estanterias::find()->where(['nombre' => 'Leído'])->one();
        $agregar->cod_estanteria = $estanteria->id;
        $agregar->save();
        
    }
}
     public function beforeSave($insert)
{
    if ($this->f_lectura) {
        $this->f_lectura = date("Y-m-d", strtotime($this->f_lectura));
    }
      if ($this->f_publi) {
        $this->f_publi = date("Y-m-d", strtotime($this->f_publi));
    }
    return parent::beforeSave($insert);
}
    
        public function afterFind()
{
    if ($this->f_lectura)
    {
        $this->f_lectura = date("d-m-Y", strtotime($this->f_lectura));
    }
    
     if ($this->f_publi)
    {
        $this->f_publi = date("d-m-Y", strtotime($this->f_publi));
    }
}
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ISBN' => 'Isbn',
            'titulo' => 'Título',
            'paginas_leidas'=>'Páginas leídas',
            'sipnosis' => 'Sipnosis',
            'num_pag' => 'Páginas',
            'f_publi' => 'Publicación',
            'cod_lector' => 'Lector',
            'f_lectura' => 'Fecha de Lectura',
            'calificacion' => 'Calificación',
        ];
    }

    /**
     * Gets query for [[Agregars]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAgregars()
    {
        return $this->hasMany(Agregar::class, ['cod_libro' => 'id']);
    }

    /**
     * Gets query for [[CodEscritors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEscritors()
    {
        return $this->hasMany(Escritores::class, ['id' => 'cod_escritor'])->viaTable('escribir', ['cod_libro' => 'id']);
    }

    /**
     * Gets query for [[CodEstanterias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEstanterias()
    {
        return $this->hasMany(Estanterias::class, ['id' => 'cod_estanteria'])->viaTable('agregar', ['cod_libro' => 'id']);
    }

    /**
     * Gets query for [[CodLector]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodLector()
    {
        return $this->hasOne(Users::class, ['id' => 'cod_lector']);
    }

    /**
     * Gets query for [[Comentarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComentarios()
    {
        return $this->hasMany(Comentarios::class, ['cod_libro' => 'id']);
    }

    /**
     * Gets query for [[Escribirs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEscribirs()
    {
        return $this->hasMany(Escribir::class, ['cod_libro' => 'id']);
    }

    /**
     * Gets query for [[Generos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGeneros()
    {
        return $this->hasMany(Generos::class, ['cod_libro' => 'id']);
    }
}
