<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "agregar".
 *
 * @property int $id
 * @property int|null $cod_libro
 * @property int|null $cod_estanteria
 *
 * @property Estanterias $codEstanteria
 * @property Libros $codLibro
 */
class Agregar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agregar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_libro', 'cod_estanteria'], 'integer'],
            [['cod_estanteria', 'cod_libro'], 'unique', 'targetAttribute' => ['cod_estanteria', 'cod_libro']],
            [['cod_estanteria'], 'exist', 'skipOnError' => true, 'targetClass' => Estanterias::class, 'targetAttribute' => ['cod_estanteria' => 'id']],
            [['cod_libro'], 'exist', 'skipOnError' => true, 'targetClass' => Libros::class, 'targetAttribute' => ['cod_libro' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cod_libro' => 'Código del Libro',
            'cod_estanteria' => 'Estantería',
        ];
    }

    /**
     * Gets query for [[CodEstanteria]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEstanteria()
    {
        return $this->hasOne(Estanterias::class, ['id' => 'cod_estanteria']);
    }

    /**
     * Gets query for [[CodLibro]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodLibro()
    {
        return $this->hasOne(Libros::class, ['id' => 'cod_libro']);
    }
}
