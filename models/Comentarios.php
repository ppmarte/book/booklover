<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comentarios".
 *
 * @property int $id
 * @property int|null $cod_libro
 * @property string|null $comentario
 *
 * @property Libros $codLibro
 */
class Comentarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comentarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_libro'], 'integer'],
            [['comentario'], 'string', 'max' => 500],
            [['cod_libro', 'comentario'], 'unique', 'targetAttribute' => ['cod_libro', 'comentario']],
            [['cod_libro'], 'exist', 'skipOnError' => true, 'targetClass' => Libros::class, 'targetAttribute' => ['cod_libro' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cod_libro' => 'Código del Libro',
            'comentario' => 'Comentario',
        ];
    }

    /**
     * Gets query for [[CodLibro]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodLibro()
    {
        return $this->hasOne(Libros::class, ['id' => 'cod_libro']);
    }
}
