<?php

namespace app\models;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $correo
 * @property string|null $pass
 * @property string $authKey
 * @property string $accessToken
 * @property string|null $f_registro
 * @property string|null $biografia
 *
 * @property Retos[] $codRetos
 * @property Libros[] $libros
 * @property Participacion[] $participacions
 */
class Users extends  ActiveRecord{
          public $password_repeat;
          
    public static function getDb()
    {
        return Yii::$app->db;
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['authKey', 'accessToken','email', 'username', 'password'], 'required', 'message' => 'Campo requerido'],
            [['f_registro'],  'safe',],
            [['f_registro'], 'default', 'value' => date("Y-m-d")],
            [['username', 'email'], 'string', 'max' => 100],
           [['username'], 'match', 'pattern' => "/^[0-9a-záéíóúÁÉÍÓÚüÜàèìòùÀÈÌÒÙ\s]+$/i", 'message' => 'Sólo se aceptan letras y números'],
            [['email'], 'email', 'message' => 'Formanto no valido'],
            [['password', 'accessToken'], 'string', 'max' => 50],
              [['password'], 'match', 'pattern' => "/^.{6,50}$/", 'message' => 'Mínimo 6 y máximo 50 caracteres'],
            [['password_repeat'], 'compare', 'compareAttribute' => 'password', 'message' => 'Los passwords no coinciden'],
            [['authKey'], 'string', 'max' => 250],
            [['biografia'], 'string', 'max' => 2500],
        ];
    }
    
     public function beforeSave($insert)
{
    if ($this->f_registro) {
        $this->f_registro = date("Y-m-d", strtotime($this->f_registro));
    }
    return parent::beforeSave($insert);
}

        public function afterFind()
{
    if ($this->f_registro)
    {
        $this->f_registro = date("d-m-Y", strtotime($this->f_registro));
    }
}
  /*public function email_existe()
    {
  
  //Buscar el email en la tabla
  $table = Users::find()->where("email=:email", [":email" => $this->email]);
  
  //Si el email existe mostrar el error
  if ($table->count() == 1)
  {
                $this->addError($this->email, "El email seleccionado existe");
  }
    }
 
    public function username_existe()
    {
  //Buscar el username en la tabla
  $table = Users::find()->where("username=:username", [":username" => $this->username]);
  
  //Si el username existe mostrar el error
  if ($table->count() == 1)
  {
                $this->addError($this->username, "El usuario seleccionado existe");
  }
    }*/
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
           
            'username' => 'Nombre',
            'email' => 'Correo',
            'password' => 'Contraseña',
            'authKey' => 'Auth Key',
            'accessToken' => 'Access Token',
            'f_registro' => 'Registro',
            'biografia' => 'Biografía',
        ];
    }

    /**
     * Gets query for [[CodRetos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodRetos()
    {
        return $this->hasMany(Retos::class, ['id' => 'cod_reto'])->viaTable('participacion', ['cod_lector' => 'id']);
    }

    /**
     * Gets query for [[Libros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLibros()
    {
        return $this->hasMany(Libros::class, ['cod_lector' => 'id']);
    }

    /**
     * Gets query for [[Participacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParticipacions()
    {
        return $this->hasMany(Participacion::class, ['cod_lector' => 'id']);
    }
}
