<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estanterias".
 *
 * @property int $id
 * @property string|null $nombre
 *
 * @property Agregar[] $agregars
 * @property Libros[] $codLibros
 */
class Estanterias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estanterias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 50],
              [['nombre'], 'match', 'pattern' => "/^[0-9a-záéíóúÁÉÍÓÚüÜàèìòùÀÈÌÒÙ\s]+$/i", 'message' => 'Sólo se aceptan letras y números'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Agregars]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAgregars()
    {
        return $this->hasMany(Agregar::class, ['cod_estanteria' => 'id']);
    }

    /**
     * Gets query for [[CodLibros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodLibros()
    {
        return $this->hasMany(Libros::class, ['id' => 'cod_libro'])->viaTable('agregar', ['cod_estanteria' => 'id']);
    }
}
