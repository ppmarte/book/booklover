<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "retos".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $f_inicio
 * @property string|null $f_fin
 * @property int|null $objetivo
 *
 * @property Lectores[] $codLectors
 * @property Participacion[] $participacions
 */
class Retos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'retos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['f_inicio', 'f_fin'], 'safe'],
            [['objetivo'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
            [['nombre'], 'unique',],
  
             [['nombre'],'match', 'pattern' =>"/^([0-9a-z\sáéíóúñÁÉÍÓÚüÜàèìòùÀÈÌÒÙÑ.]+)$/i", 'message' => 'Sólo se aceptan letras y números'],
        ];
    }
    
    /*metodo para guardar las fechas en el formato correcto*/
     public function beforeSave($insert)
        {
                if ($this->f_inicio) {
                    $this->f_inicio = date("Y-m-d", strtotime($this->f_inicio));
                }
                 if ($this->f_fin) {
                    $this->f_fin = date("Y-m-d", strtotime($this->f_fin));
                }
                return parent::beforeSave($insert);
        }
        
        /*Metodo para mostrar la fecha en un formato apto para el usuario*/
        public function afterFind()
        {
                if ($this->f_inicio)
                {
                    $this->f_inicio = date("d-m-Y", strtotime($this->f_inicio));
                }

                 if ($this->f_fin)
                {
                    $this->f_fin = date("d-m-Y", strtotime($this->f_fin));
                }
        }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
      
            'nombre' => 'Nombre',
            'f_inicio' => 'Inicio',
            'f_fin' => 'Fin',
            'objetivo' => 'Objetivo',
        ];
    }

    /**
     * Gets query for [[CodLectors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodLectors()
    {
        return $this->hasMany(Users::class, ['id' => 'cod_lector'])->viaTable('participacion', ['cod_reto' => 'id']);
    }

    /**
     * Gets query for [[Participacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParticipacions()
    {
        return $this->hasMany(Participacion::class, ['cod_reto' => 'id']);
    }
}
