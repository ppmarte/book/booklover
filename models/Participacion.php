<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "participacion".
 *
 * @property int $id
 * @property int|null $cod_lector
 * @property int|null $cod_reto
 *
 * @property Lectores $codLector
 * @property Retos $codReto
 */
class Participacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'participacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_lector', 'cod_reto'], 'integer'],
            [['cod_lector', 'cod_reto'], 'unique', 'targetAttribute' => ['cod_lector', 'cod_reto']],
            [['cod_lector'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['cod_lector' => 'id']],
            [['cod_reto'], 'exist', 'skipOnError' => true, 'targetClass' => Retos::class, 'targetAttribute' => ['cod_reto' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cod_lector' => 'Lector',
            'cod_reto' => 'Reto',
        ];
    }

    /**
     * Gets query for [[CodLector]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodLector()
    {
        return $this->hasOne(Users::class, ['id' => 'cod_lector']);
    }

    /**
     * Gets query for [[CodReto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodReto()
    {
        return $this->hasOne(Retos::class, ['id' => 'cod_reto']);
    }
}
