<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "escritores".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $f_nacimiento
 * @property string|null $lugar_nacimiento
 * @property string|null $biografia
 *
 * @property Libros[] $codLibros
 * @property Escribir[] $escribirs
 */
class Escritores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'escritores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['f_nacimiento'], 'safe', ],
            [['nombre', 'lugar_nacimiento'], 'string', 'max' => 50],
            ['lugar_nacimiento', 'match', 'pattern' => "/^([a-z\sáéíóúñÁÉÍÓÚüÜàèìòùÀÈÌÒÙÑ.]+)$/i"],
            ['nombre', 'match', 'pattern' => "/^([a-z\sáéíóúñÁÉÍÓÚüÜàèìòùÀÈÌÒÙÑ.]+)$/i"],
            ['nombre', 'unique', 'targetClass' => 'app\models\Escritores', 'message' => 'Este nombre ya está en uso'],
            [['biografia'], 'string', 'max' => 2500],
        ];
    }

    public function beforeSave($insert)
{
    if ($this->f_nacimiento) {
        $this->f_nacimiento = date("Y-m-d", strtotime($this->f_nacimiento));
    }
    return parent::beforeSave($insert);
}
    
    public function afterFind()
{
    if ($this->f_nacimiento)
    {
        $this->f_nacimiento = date("d-m-Y", strtotime($this->f_nacimiento));
    }
}

public function nombre_existe()
{
    // Verificar si el nombre ya existe en la base de datos
    $existe = Escritores::find()->select('id')->where(['nombre' => $this->nombre])->exists();

    // Si el nombre existe, agregar un error al modelo
    if ($existe) {
        $this->addError('nombre', 'El autor ya existe en la base de datos.');
    }
}
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'f_nacimiento' => 'Fecha de nacimiento',
            'lugar_nacimiento' => 'Nacionalidad',
            'biografia' => 'Biografia',
        ];
    }

    /**
     * Gets query for [[CodLibros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodLibros()
    {
        return $this->hasMany(Libros::class, ['id' => 'cod_libro'])->viaTable('escribir', ['cod_escritor' => 'id']);
    }

    /**
     * Gets query for [[Escribirs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEscribirs()
    {
        return $this->hasMany(Escribir::class, ['cod_escritor' => 'id']);
    }
    
    
    
    public static function getEscritoresOptions()
{
    $escritores = self::find()->orderBy('nombre')->all();
    return \yii\helpers\ArrayHelper::map($escritores, 'id', 'nombre');
}
}

