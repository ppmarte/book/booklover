<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Estanterias $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="estanterias-form">

 <?php $form = ActiveForm::begin();
?>

    <?= $form->field($model, 'nombre')->textInput(['placeholder' => 'Escribe el nombre de la estantería']) ?>

   <div class="form-group">
         <?= Html::resetButton('Borrar', ['class' => 'btn btn-secondary btn-default']) ?>
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>
