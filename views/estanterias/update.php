<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Estanterias $model */

$this->title = 'Actualizar Estanterias: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Estanterias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="estanterias-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
