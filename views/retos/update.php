<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Retos $model */

$this->title = 'Actualizar Retos: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Retos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="retos-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
