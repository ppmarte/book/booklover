<?php

use app\models\Retos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Retos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="retos-index">

    <h2><?= Html::encode($this->title) ?></h2>

    <p>
        <?= Html::a('Nuevo Reto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'id',
            'nombre',
            'f_inicio',
            'f_fin',
            'objetivo',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Retos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
