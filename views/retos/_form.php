<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/** @var yii\web\View $this */
/** @var app\models\Retos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="retos-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group">
        <?= $form->field($model, 'nombre')->textInput(['maxlength' => true,'placeholder' => 'Escribe nombre del reto']) ?>
       </div> 

    <div class="form-group">
        <?= $form->field($model, 'f_inicio')  ->widget(DatePicker::classname(), [
            'name'=>'f_inicio',
           'options' => ['placeholder' => 'Seleccione una fecha'],
          'pluginOptions' => [
            'format' => 'yyyy-m-dd',
            'todayHighlight' => true,
            'autoclose'=>true,
            'convertFormat' => true, // convierte automáticamente el formato devuelto al formato utilizado para mostrar la fecha
            ]

        ]) //. $addon;

        ?>
    </div>
    <div class="form-group">
        <?= $form->field($model, 'f_fin') ->widget(DatePicker::classname(), [
            'name'=>'f_inicio',
           'options' => ['placeholder' => 'Seleccione una fecha'],
            'pluginOptions' => [
            'format' => 'yyyy-m-dd',
            'todayHighlight' => true,
            'autoclose'=>true,
            'convertFormat' => true, // convierte automáticamente el formato devuelto al formato utilizado para mostrar la fecha
                ]
            ])  ?>

         </div>
        <div class="form-group">
            <?= $form->field($model, 'objetivo')->textInput(['placeholder' => 'Escribe número de libros para cumplir el reto']) ?>
       </div>
        <div class="form-group">
             <?= Html::resetButton('Borrar', ['class' => 'btn btn-secondary btn-default']) ?>
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
        </div>


    <?php ActiveForm::end(); ?>

</div>
