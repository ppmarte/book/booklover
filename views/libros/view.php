<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use kartik\widgets\StarRating;


/** @var yii\web\View $this */
/** @var app\models\Libros $model */

$this->title = $model->titulo;
$this->params['breadcrumbs'][] = ['label' => 'Libros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>
<div class="libros-view">
    <div class="lateral"></div>
            <div class="libros">
                 <?= Html::img('@web/img/'.$model -> id.'.jpg', ['alt'=>'Portada', 'class'=>'libro-cover'])?>
                <h2><?=$model -> titulo?></h2>
                    <section>  
                           <div class="autores" >
                           <?= implode('</div><div  class="autores" >', ArrayHelper::getColumn($model->codEscritors, 'nombre')) ?></div>
                     </section>  
                     <section>  
                           <div class="genero" >
                           <?= implode('</div><div  class="genero" >', ArrayHelper::getColumn($model->generos, 'genero')) ?></div>
                      </section>     
                        <section>  
                               <?=StarRating::widget([
                                       'name' => 'rating_33',
                                       'value' => $model->calificacion,
                                       'pluginOptions' => [
                                           'readonly' => true,
                                           'showClear' => false,
                                           'showCaption' => true,
                                            'language' => 'es',
                                       ],
                                   ]);?>   
                        </section>  
                       <section class="sipnosis">  
                          <?=$model -> sipnosis?>
                         <section>  
              
                             
                              <section>  
                    <div class="comentario" >
                    <?= implode('</div><div  class="comentario" >', ArrayHelper::getColumn($model->comentarios, 'comentario')) ?></div>
               </section>     
            </div>

  

</div>
