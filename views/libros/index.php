<?php

use app\models\Libros;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Libros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="libros-index">

    <h2><?= Html::encode($this->title) ?></h2>

    <p>
        <?= Html::a('Nuevo Libro', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
        
            'id',
            'ISBN',
            'titulo',
          
                [
            'attribute' => 'sipnosis',
            'format' => 'raw',
        ],
            'num_pag',
            'f_publi',
            //'cod_lector',
            //'f_lectura',
            //'calificacion',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Libros $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
