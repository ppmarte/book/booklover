<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\editors\Summernote;

/** @var yii\web\View $this */
/** @var app\models\Comentarios $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="comentarios-form">

   <?php $form = ActiveForm::begin();
?>

    <?= $form->field($model, 'cod_libro')->textInput(['placeholder' => 'Escribe el código del libro']) ?>

  <div class="form-group">
    <?= $form->field($model, 'comentario')->widget(Summernote::class, [
 'useKrajeePresets' => true,
        'pluginOptions'=>[
    'height' => 200,
    'dialogsFade' => true,
    'toolbar' => [
        ['style1', ['style']],
        ['style2', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript']],
        ['font', ['fontname', 'fontsize', 'color', 'clear']],
        ['para', ['ul', 'ol', 'paragraph', 'height']],
        ['insert', ['link', 'picture', 'video', 'table', 'hr']],
    ],
    'fontSizes' => ['8', '9', '10', '11', '12', '13', '14', '16', '18', '20', '24', '36', '48'],
    'codemirror' => [
      //  'theme' => Codemirror::DEFAULT_THEME,
        'lineNumbers' => true,
        'styleActiveLine' => true,
        'matchBrackets' => true,
        'smartIndent' => true,
        'enablePrettyFormat'=>false,
        'autoFormatCode'=>false,
        
    ],
               'placeholder' => 'Escribe tu comentario',
]

]);?>
</div>

   <div class="form-group">
         <?= Html::resetButton('Borrar', ['class' => 'btn btn-secondary btn-default']) ?>
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>
