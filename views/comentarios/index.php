<?php

use app\models\Comentarios;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Comentarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comentarios-index">

    <h2><?= Html::encode($this->title) ?></h2>

    <p>
        <?= Html::a('Nuevo Comentario', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'cod_libro',
             [
            'attribute' => 'comentario',
            'format' => 'raw',
        ],
            
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Comentarios $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
