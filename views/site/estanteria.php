<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use  yii\widgets\ListView;
use yii\grid\ActionColumn;
use yii\grid\GridView;


$this->params['breadcrumbs'][] = ['label' => 'Estanteria: ', 'url' => ['estanterias/index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);


?>

    <h2><?= Html::encode($this->title) ?></h2>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'id',
            'label' => 'Portada',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a(Html::img('@web/img/'.$model->id.'.jpg', ['alt'=>'Portada', 'class'=>'minibook-cover']),['libros/view', 'id'=>$model->id]);
            },
        ],
        [
    'attribute' => 'titulo',
    'label' => 'Título',
    'format' => 'raw', // Asegura que se interprete el HTML
    'value' => function ($model) {
        return Html::decode(Html::a($model->titulo, ['libros/view', 'id' => $model->id]));
    },
],

                [
    'attribute' => 'autor',
'label' => 'Autor',
'value' => function ($model) {
    $nombres = [];
    foreach ($model->codEscritors as $escritor) {
        $nombres[] = Html::a($escritor->nombre, ['escritores/view', 'id' => $escritor->id]);
    }
    return implode(', ', $nombres);
},
'format' => 'raw',


    ],
      [
            'attribute' => 'fecha',
            'label' => 'Fecha de lectura',
            'value' => function ($model) {
                return $model->f_lectura;
            },
        ],           
          [
            'attribute' => 'calificacion',
            'label' => 'Calificación',
            'value' => function ($model) {
                return $model->calificacion;
            },
        ],    
                    

    ],
]); ?>


<div class="jumbotron text-center bg-transparent">
   <?=   ListView::widget([
            'dataProvider' => $dataProvider,
           'itemView' => '_estante',
          'layout'=>"{items}",
       
        ]);
?>
    
  
    </div>