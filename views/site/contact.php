<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap4\ActiveForm $form */
/** @var app\models\ContactForm $model */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\captcha\Captcha;

$this->title = 'Contacta con nosotros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h2><?= Html::encode($this->title) ?></h2>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Gracias por contactar con nosotros. Responderemos lo antes posible.
        </div>

        

    <?php else: ?>

        <p>
           Si tienes alguna duda puedes ponerte en contacto con nosotros, sólo tienes que rellenar el siguiente formulario.
        </p>

        <div class="row">
            <div class="col-lg-5">

                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                    <?= $form->field($model, 'name')->label('Nombre')->textInput(['autofocus' => true,'placeholder' => 'Escribre tu nombre']) ?>

                    <?= $form->field($model, 'email') ->textInput(['autofocus' => true,'placeholder' => 'Escribre tu correo']) ?>

                    <?= $form->field($model, 'subject')->label('Asunto') ->textInput(['autofocus' => true,'placeholder' => 'Escribre el asunto']) ?>

                    <?= $form->field($model, 'body')->label('Tu mensaje')->textarea(['rows' => 6,'autofocus' => true,'placeholder' => 'Escribre tu duda']) ?>

                    <?= $form->field($model, 'verifyCode')->label('Código de verificación')->widget(Captcha::className(), [
                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                    ]) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    <?php endif; ?>
</div>
