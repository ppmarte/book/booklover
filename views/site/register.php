<?php
use yii\helpers\Html;
use kartik\date\DatePicker;
use yii\bootstrap4\ActiveForm;
use kartik\editors\Summernote;


$this->title = 'Nuevo Usuario';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-create"> 

 <h2><?= Html::encode($this->title) ?></h2>
<?php $form = ActiveForm::begin([
    'method' => 'post',
    'id' => 'formulario',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
]);
?>
<div class="form-group">
 <?= $form->field($model, "username")->textInput(['placeholder' => 'Introduce tu nombre de usuario']) ?>   
</div>

<div class="form-group">
 <?= $form->field($model, "email")->textInput(['placeholder' => 'Introduce tu correo electrónico']) ?>   
</div>

<div class="form-group">
<?= $form->field($model, "password")->passwordInput(['placeholder' => 'Introduce tu contraseña']) ?>
  
</div>

<div class="form-group">
 <?= $form->field($model, "password_repeat")->passwordInput(['placeholder' => 'Repite tu contraseña'])  ?>   
</div>

<div class="form-group">
<?= $form->field($model, 'f_registro', [
   'labelOptions' => ['label' => 'Fecha de registro']
])->widget(DatePicker::classname(), [
    'name'=>'fecha de registro',
    'convertFormat'=>true,
    'disabled' => true,
    'options' => [
        'value' => date('Y-m-d'),
    ]
]) ?>
</div>



<div class="form-group">
     <?= $form->field($model, 'biografia')->widget(Summernote::class, [
 'useKrajeePresets' => true,
        'pluginOptions'=>[
    'height' => 200,
    'dialogsFade' => true,
    'toolbar' => [
        ['style1', ['style']],
        ['style2', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript']],
        ['font', ['fontname', 'fontsize', 'color', 'clear']],
        ['para', ['ul', 'ol', 'paragraph', 'height']],
        ['insert', ['link', 'picture', 'video', 'table', 'hr']],
    ],
    'fontSizes' => ['8', '9', '10', '11', '12', '13', '14', '16', '18', '20', '24', '36', '48'],
    'codemirror' => [
      //  'theme' => Codemirror::DEFAULT_THEME,
        'lineNumbers' => true,
        'styleActiveLine' => true,
        'matchBrackets' => true,
        'smartIndent' => true,
        'enablePrettyFormat'=>false,
        'autoFormatCode'=>false,
        
    ],
             'placeholder' => 'Ingrese su biografía aquí',
]

]);?>

</div>


 <div class="form-group">
        <?= Html::resetButton('Borrar', ['class' => 'btn btn-secondary btn-default']) ?>
      <?= Html::submitButton("Registrarse", ["class" => "btn btn-primary"]) ?>
    </div>


<?php $form->end() ?>

 </div>
