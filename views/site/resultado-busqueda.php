<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use  yii\widgets\ListView;
$progress=70;
//$user_id = Yii::$app->user->identity->id;
$this->title = 'Búsqueda';
?>
    <div class="jumbotron text-center bg-transparent">

    <h2>Resultados de la búsqueda para "<?= Html::encode($busqueda) ?>"</h2>
  <div class="resultado-busqueda">
 
    
<?php foreach ($registros as $registro): ?>
<div class="col-sm-2">
            <div class="card minimo">
        <?= Html::a(Html::img('@web/img/'.$registro -> id.'.jpg', ['alt'=>'Portada', 'class'=>'book-cover']),['libros/view', 'id'=>$registro-> id ] ) ?>
        
        <h6><?= Html::a($registro->titulo, ['libros/view', 'id'=>$registro-> id ] ) ?> </h6>
        <?php if (!empty($registro->codEscritors)): ?>
            <?= Html::a($registro->codEscritors[0]->nombre, ['escritores/view', 'id' => $registro->codEscritors[0]->id]) ?>
        <?php else: ?>
            No se encontró el escritor
        <?php endif; ?>
   </div>
        </div>
<?php endforeach; ?>
 
</div>
</div>
