<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use  yii\widgets\ListView;
use  yii\widgets\LinkPager;

//$user_id = Yii::$app->user->identity->id;
$this->title = 'Book';
?>

         <?php  if (Yii::$app->user->isGuest) {?>
       <div class="col-12 justify-content-center">
        
    <div class="jumbotron text-center bg-transparent">
        
       
        <h1 class="display-4">Nuestros libros</h1>

      <?=   ListView::widget([
            'dataProvider' => $dataProvider,
          'pager' => [
        'firstPageLabel' => 'first',
        'lastPageLabel' => 'last',
        'prevPageLabel' => 'previous',
        'nextPageLabel' => 'next',
    ],
           'itemView' => '_libro',
            'layout'=>"\n{items}\n{pager}",
            
        ]);
?>
   
    </div>   </div>
        
  <?php }else{?> 
           <div class="row justify-content-evenly align-items-top ">
               
     <div class="col-3 sidebard"> 
 
        <h5 class="d-flex align-items-center  text-decoration-none border-bottom">Estanterías</h5>
        
              <div class="collapse show" id="home-collapse">
             
                       
                       <?= Html::a('Todos los libros', ['site/estanteriatotal']) ?> 

                    <?php foreach ($estanterias as $estanteria): ?>
                       <br>
              <?= Html::a($estanteria->nombre, ['site/estanteria', 'id' => $estanteria->id]) ?>

                                       
                    <?php endforeach; ?>
                
              </div>
       
        
        
        <h5 class="d-flex align-items-center link-dark text-decoration-none border-bottom">Lecturas actuales</h5>
        
    <div class="actual">
    <?php foreach ($librosLeyendoActualmente as $libro): ?>
        <div class="libro" data-libro-id="<?=$libro->id?>">
            <?= Html::img('@web/img/'.$libro->id.'.jpg', ['alt'=>'Portada', 'class'=>'leyendo-cover']) ?>
            <section>
                <h6><?= Html::a($libro->titulo, ['libros/view', 'id' => $libro->id]) ?></h6>
                <p>de <?= Html::a($libro->getCodEscritors()->one()->nombre, ['escritores/view', 'id' => $libro->getCodEscritors()->one()->id]) ?></p>
                <?php $porcentajeLeido = $libro->porcentaje_leido($libro->paginas_leidas); ?>

               <div class="progress" id="progresoLibro<?= $libro->id ?>">
 
    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" 
         style="width: <?= $porcentajeLeido ?>%" 
         aria-valuenow="<?= $libro->paginas_leidas ?>" aria-valuemin="0" aria-valuemax="<?= $libro->num_pag ?>">
         
    </div>
                  
</div>
<div id="texto-progreso<?= $libro->id ?>">
                        <?= $libro->paginas_leidas ?> / <?= $libro->num_pag ?> (<?= round($libro->porcentaje_leido($libro->paginas_leidas)) ?>%)
</div>



<!-- Botón que abre el modal -->
<?= Html::button('Actualizar', [
    'class' => 'btn btn-outline-secondary btn-sm',
    'data-toggle' => 'modal',
    'data-target' => '#actualizarModal' . $libro->id,
    
]) ?>


<!-- Modal correspondiente a cada libro -->
<div class="modal fade" id="actualizarModal<?= $libro->id ?>" tabindex="-1" role="dialog" 
     aria-labelledby="actualizarModalLabel<?= $libro->id ?>" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="actualizarModalLabel<?= $libro->id ?>">Actualizar páginas leídas de <?= $libro->titulo ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <?= Html::label('Páginas leídas') ?>
                    <?= Html::input('number', 'paginasLeidas', $libro->paginas_leidas, ['class' => 'form-control', 'id' => 'paginasLeidas' . $libro->id]) ?>
                </div>
                <button class="btn-actualizar" class="btn btn-outline-secondary btn-sm" data-toggle="modal" data-target="#actualizarModal<?= $libro->id ?>" data-id="<?= $libro->id ?>" data-paginas="<?= $libro->num_pag ?>">Actualizar</button>
            </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

            <?php endforeach; ?>
        </div>


         <h5 class="d-flex align-items-center link-dark text-decoration-none border-bottom">Generos</h5>
  </div>
                <div class="col-1"> 
                </div>
    <div class="col-8 justify-content-center">
          <div class="jumbotron text-center bg-transparent">
        
       
        <h1 class="display-4">Nuestros libros</h1>

            <?=   ListView::widget([
                  'dataProvider' => $dataProvider,
                   'pager' => [
              
    'class' => \yii\bootstrap4\LinkPager::class,
        'firstPageLabel' => '«',
        'lastPageLabel' => '»,',
        'prevPageLabel' => '<',
        'nextPageLabel' => '>',
    ],
                 'itemView' => '_libro',
                  'layout'=>"\n{items}",

              ]);
            ?>

        </div>
     </div>
 </div>
           
     <?php }?> 