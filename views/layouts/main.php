<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use app\assets\FontAsset;
use yii\helpers\Url;
use yii\web\JsExpression;
$baseUrl = Url::base();
define('BUSQUEDA_AJAX_URL', Url::to(['controller/actionBusquedaajax']));

AppAsset::register($this);
FontAsset::register($this);

$this->registerJsFile(
    '@web/js/main.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>

<script>
    var baseUrl = '<?= $baseUrl ?>';
</script>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <meta charset="<?= Yii::$app->charset ?>">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
   <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
        ],
    ]);
    
    
     $navItems = [       
          '<form method="GET">
  <li>
    <span class="buscar">
      <input type="text" id="buscar" placeholder="Buscar" onkeydown="if (event.keyCode == 13) { buscar_ahora(); return false; }" 
      autocomplete="off" onkeyup="buscar_en_tiempo_real();" oninput="limpiar_resultados();"/>
      <div id="resultados-busqueda"></div>
      <div class="btnsearch" onclick="buscar_ahora();">
        <i class="fas fa-search icon"></i>
      </div>
    </span>
  </li>
</form>',
         ['label' => 'Inicio', 'url' => ['/site/index']],
        ['label' => 'Mis libros', 'url' => ['/site/index']],
        [            'label' => 'Géneros',            'items' => [                ['label' => 'Agregar', 'url' => ['/agregar/index']],
                ['label' => 'Comentarios', 'url' => ['/comentarios/index']],
                ['label' => 'Escribir', 'url' => ['/escribir/index']],
                ['label' => 'Escritores', 'url' => ['/escritores/index']],        
                ['label' => 'Estanterias', 'url' => ['/estanterias/index']],       
                ['label' => 'Generos', 'url' => ['/generos/index']],
                ['label' => 'Libros', 'url' => ['/libros/index']],
                ['label' => 'Participacion', 'url' => ['/participacion/index']],
                ['label' => 'Retos', 'url' => ['/retos/index']],
                ['label' => 'Usuarios', 'url' => ['/users/index']],
            ],
            'options' => ['class' => 'dropdown'],
        ],
        ['label' => 'Contacto', 'url' => ['/site/contact']],
        '<button class="switch" id="switch">
            <span><i class="fas fa-sun"></i></span>
            <span><i class="fas fa-moon"></i></span>
        </button>',
       
    ];

    if (Yii::$app->user->isGuest) {
        array_push($navItems, ['label' => 'Conectarse', 'url' => ['/site/login']], ['label' => 'Registrarse', 'url' => ['/site/register']]);
    } else {
        array_push($navItems, ['label' => 'Logout (' . Yii::$app->user->identity->username . ')',            'url' => ['/site/logout'],
            'linkOptions' => ['data-method' => 'post']]
        );
    }

    echo Nav::widget([       
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $navItems,       
    ]);
    NavBar::end();
?>


</header>

<main role="main" class="flex-shrink-0">
    
        <div class="container">
            
  
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
  </div>

</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-left">&copy; Good Book <?= date('Y') ?></p>
        <p class="float-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
