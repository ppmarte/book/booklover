<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ActiveField;
use \yii\widgets\InputWidget;
use kartik\date\DatePicker;
use kartik\editors\Summernote;

/** @var yii\web\View $this */
/** @var app\models\Escritores $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="escritores-form">

    <?php $form = ActiveForm::begin();
?>
<div class="form-group">
    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Escribe el nombre']) ?>
</div>

<div class="form-group">
    <?= $form->field($model, 'f_nacimiento',  [
   'labelOptions' => ['label' => 'Fecha de nacimiento']]
    //  'addon'=>['prepend'=>['content'=>'<i class="fas fa-calendar-alt"></i>']],
      
    )->widget(DatePicker::classname(), [
  'name'=>'f_nacimiento',
 'options' => ['placeholder' => 'Seleccione una fecha'],
    'pluginOptions' => [
        'format' => 'yyyy-m-dd',
        'todayHighlight' => true,
        'autoclose'=>true,
        'convertFormat' => true, // convierte automáticamente el formato devuelto al formato utilizado para mostrar la fecha
    ]
    
]) //. $addon;
       
?>
</div>

<div class="form-group">
    <?= $form->field($model, 'lugar_nacimiento')->textInput(['maxlength' => true,'placeholder' => 'Escribe la nacionalidad']) ?>
</div>

<div class="form-group">
    <?= $form->field($model, 'biografia')->widget(Summernote::class, [
 'useKrajeePresets' => true,
        'pluginOptions'=>[
    'height' => 200,
    'dialogsFade' => true,
    'toolbar' => [
        ['style1', ['style']],
        ['style2', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript']],
        ['font', ['fontname', 'fontsize', 'color', 'clear']],
        ['para', ['ul', 'ol', 'paragraph', 'height']],
        ['insert', ['link', 'picture', 'video', 'table', 'hr']],
    ],
    'fontSizes' => ['8', '9', '10', '11', '12', '13', '14', '16', '18', '20', '24', '36', '48'],
    'codemirror' => [
      //  'theme' => Codemirror::DEFAULT_THEME,
        'lineNumbers' => true,
        'styleActiveLine' => true,
        'matchBrackets' => true,
        'smartIndent' => true,
        'enablePrettyFormat'=>false,
        'autoFormatCode'=>false,
        
    ],
               'placeholder' => 'Ingrese la biografía',
]

]);?>
</div>

   <div class="form-group">
         <?= Html::resetButton('Borrar', ['class' => 'btn btn-secondary btn-default']) ?>
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

 
    <?php ActiveForm::end(); ?>

</div>
