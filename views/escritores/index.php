<?php

use app\models\Escritores;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use  yii\widgets\ListView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Escritores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="escritores-index">

    <h2><?= Html::encode($this->title) ?></h2>

    <p>
        <?= Html::a('Nuevo Escritor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

     

            <?=   ListView::widget([
                  'dataProvider' => $dataProvider,
                 'itemView' => '_autor',
                  'layout'=>"\n{items}",
      
              ]);
            ?>
   
     

  


</div>
