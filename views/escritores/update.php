<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Escritores $model */

$this->title = 'Actualizar Escritores: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Escritores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="escritores-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
