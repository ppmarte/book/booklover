<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Escritores $model */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Escritores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="escritores-view">

    <h2><?= Html::encode($this->title) ?></h2>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro que quiere borrar este elemento?',
                'method' => 'post',
            ],
          

        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nombre',
            'f_nacimiento',
            'lugar_nacimiento',
                   [
            'attribute' => 'biografia',
            'format' => 'raw',
        ],
            
        ],
    ]) ?>

</div>
