<?php

use app\models\Escribir;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Escribir';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="escribir-index">

    <h2><?= Html::encode($this->title) ?></h2>

    <p>
        <?= Html::a('Nuevo Escribir', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'cod_escritor',
            'cod_libro',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Escribir $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
