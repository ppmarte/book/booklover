<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Escribir $model */

$this->title = 'Actualizar Escribir: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Escribir', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="escribir-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
