<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Escribir $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="escribir-form">

<?php $form = ActiveForm::begin();
?>

    <?= $form->field($model, 'cod_escritor')->textInput(['placeholder' => 'Escribe el código del escritor']) ?>

    <?= $form->field($model, 'cod_libro')->textInput(['placeholder' => 'Escribe el código del libro']) ?>

   <div class="form-group">
         <?= Html::resetButton('Borrar', ['class' => 'btn btn-secondary btn-default']) ?>
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>
