<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Escribir $model */

$this->title = 'Nuevo Escribir';
$this->params['breadcrumbs'][] = ['label' => 'Escribir', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="escribir-create">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
