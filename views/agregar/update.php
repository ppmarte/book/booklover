<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Agregar $model */

$this->title = 'Actualizar estantería de ' . $model->codEstanteria->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Agregar', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' =>  $model->codEstanteria->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="agregar-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
