<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Agregar $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="agregar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_libro')->textInput(['placeholder' => 'Escribe el código del libro']) ?>

    <?= $form->field($model, 'cod_estanteria')->textInput(['placeholder' => 'Escribe el código de la estantería']) ?>

     <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?> 
        <?= Html::resetButton('Reset', ['class' => 'btn btn-secondary btn-default']) ?>
    </div>
 

    <?php ActiveForm::end(); ?>

</div>
