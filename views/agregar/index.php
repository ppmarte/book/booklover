<?php

use app\models\Agregar;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Agregar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agregar-index">

    <h2><?= Html::encode($this->title) ?></h2>

    <p>
        <?= Html::a('Agregar nuevo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'cod_libro',
            'cod_estanteria',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Agregar $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
