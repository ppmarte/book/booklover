<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Agregar $model */

$this->title = 'Nuevo agregar';
$this->params['breadcrumbs'][] = ['label' => 'Agregars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agregar-create">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
