<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;


/** @var yii\web\View $this */
/** @var app\models\Users $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="users-form">

<?php $form = ActiveForm::begin();
?>

    <div class="form-group">
  <?= $form->field($model, "username")->textInput(['placeholder' => 'Introduce tu nombre de usuario']) ?> 
</div>

<div class="form-group">
 <?= $form->field($model, "email")->input("email") ?>   
</div>

<div class="form-group">
 <?= $form->field($model, "password")->input("password") ?>   
</div>
    
 <div class="form-group">
 <?= $form->field($model, "password_repeat")->input("password") ?>   
</div>

<div class="form-group">
 <?= $form->field($model, "biografia")->textarea(['rows' => 3])?>   
</div>


    <div class="form-group">
         <?= Html::resetButton('Borrar', ['class' => 'btn btn-secondary btn-default']) ?>
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>
