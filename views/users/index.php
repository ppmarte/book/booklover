<?php

use app\models\Users;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Usuarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h2><?= Html::encode($this->title) ?></h2>

    <p>
        <?= Html::a('Nuevo Usuario', ['site/register'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
           
            'id',
            'username',
            // 'correo',
            // 'pass',
            // 'authKey',
            //'accessToken',
            'f_registro',
              [
            'attribute' => 'biografia',
            'format' => 'raw',
        ],
           
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Users $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
