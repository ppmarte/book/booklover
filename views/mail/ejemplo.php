<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap4\ActiveForm $form */
/** @var app\models\ContactForm $model */


use yii\bootstrap4\Html;

?>

<h2>Bienvenido <?= $usuario ?> a mi aplicación</h2>
<p>Gracias por unirte a nuestra comunidad.</p>