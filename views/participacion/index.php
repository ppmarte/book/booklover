<?php

use app\models\Participacion;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Participaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="participacion-index">

    <h2><?= Html::encode($this->title) ?></h2>

    <p>
        <?= Html::a('Nueva Participación', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'cod_lector',
            'cod_reto',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Participacion $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
