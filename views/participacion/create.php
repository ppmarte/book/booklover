<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Participacion $model */

$this->title = 'Nueva Participación';
$this->params['breadcrumbs'][] = ['label' => 'Participacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="participacion-create">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
