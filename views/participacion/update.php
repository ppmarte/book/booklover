<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Participacion $model */

$this->title = 'Actualizar Participacion: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Participacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="participacion-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
