<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Participacion $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="participacion-form">

    <?php $form = ActiveForm::begin(); ?>
<div class="form-group">
    <?= $form->field($model, 'cod_lector')->textInput([ 'placeholder' => 'Escribe el código del usuario']) ?>
</div>
<div class="form-group">
    <?= $form->field($model, 'cod_reto')->textInput([ 'placeholder' => 'Escribe el código del reto']) ?>
</div>
    <div class="form-group">
         <?= Html::resetButton('Borrar', ['class' => 'btn btn-secondary btn-default']) ?>
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
