

var id = null;


const btnSwitch = document.querySelector('#switch');

// Manejador de evento para el botón de interruptor de modo
btnSwitch.addEventListener('click', () => {
  // Alterna la clase 'dark' en el cuerpo del documento
  document.body.classList.toggle('dark');
  // Alterna la clase 'active' en el botón de interruptor
  btnSwitch.classList.toggle('active');

  // Almacena la elección del modo en el almacenamiento local
  localStorage.setItem('modo', document.body.classList.contains('dark') ? 'dark' : 'light');
});

// Manejador de evento para cargar la página
window.addEventListener('load', () => {
  // Recupera la elección del modo del almacenamiento local
  const modo = localStorage.getItem('modo');
  // Aplica la elección del modo al cuerpo del documento y al botón de interruptor correspondiente
  if (modo === 'dark') {
    document.body.classList.add('dark');
    btnSwitch.classList.add('active');
  } else if (modo === 'light') {
    document.body.classList.remove('dark');
    btnSwitch.classList.remove('active');
  }
});



function buscar_ahora() {
  var busqueda = document.getElementById("buscar").value.trim(); // Obtener la búsqueda y eliminar los espacios en blanco al inicio y al final
  if (busqueda.length > 0) { // Verificar que la búsqueda no esté vacía
    window.location.href = baseUrl + '/site/buscar?q=' + encodeURIComponent(busqueda); // Codificar la búsqueda antes de enviarla al controlador
  }
}

function buscar_en_tiempo_real() {
  var busqueda = document.getElementById("buscar").value;
  var texto_busqueda = document.getElementById("buscar").value;
  var resultados = document.getElementById("resultados-busqueda");
  
  if (busqueda.length >= 3) {
  $.ajax({
url:baseUrl + '/site/busquedaajax?q=' + busqueda,
    type: 'GET',
    data: {
      'q': busqueda
    },
success: function(response) {
  var registros = response.registros;
  $('#resultados-busqueda').empty();
  if (registros.length === 0) {
    $('#resultados-busqueda').text('No se encontraron resultados');
  } else {
    for (var i = 0; i < registros.length; i++) {
      var registro = registros[i];
      var escritores = registro.escritores;
      var div = $('<div>');
       //var enlace = $('<a>').attr('href', '' + registro.id).text(registro.titulo);
      var link = $('<a>').attr('href', baseUrl+'/libros/view?id=' + registro.id).text(registro.titulo);
      div.append(link);
      if (escritores && escritores.length > 0) {
        div.append(' - ' + escritores[0].nombre);
      }
  
     $('#resultados-busqueda').append(div);
          }
        }
      }
    });

    // Agregar evento 'click' al documento para cerrar la zona de resultados de búsqueda cuando se hace clic fuera de ella
    document.addEventListener('click', function(event) {
      if (event.target != resultados && event.target != document.getElementById("buscar")) {
        resultados.style.display = 'none';
        
      }
    });
  } else {
      resultados.innerHTML = "Introduce al menos 3 caracteres para buscar";
  }
  
  // Agregar evento 'click' al cuadro de búsqueda para mostrar la zona de resultados
  document.getElementById("buscar").addEventListener('click', function(event) {
    if (resultados.style.display === 'none' && document.getElementById("buscar").value.length >= 3) {
      resultados.style.display = 'block';
    }
  });
}


function limpiar_resultados() {
  document.getElementById("resultados-busqueda").innerHTML = "";
}

// Obtiene el progreso actual del libro
function obtenerProgreso() {
    var idLibro = window.libroId;
    var url = '/estanterias/progreso-libro?id=' + idLibro;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, false);
    xhr.send();
    return xhr.responseText;
}



function actualizarTextoProgreso(paginasLeidas, numPaginas, idLibro) {
  var porcentajeLeido = Math.round(paginasLeidas / numPaginas * 100);
    var textoProgreso = paginasLeidas + ' / ' + numPaginas + ' pág. (' + porcentajeLeido + '%)';
    document.getElementById('texto-progreso'+ idLibro).textContent = textoProgreso;
}


function actualizarProgreso() {
    var progreso = obtenerProgreso();
    var datos = JSON.parse(progreso);
    var porcentaje = datos.paginas_leidas / window.numPaginas * 100;
    var progresoBarra = document.querySelector('.progress-bar');
    progresoBarra.style.width = porcentaje + '%';
    progresoBarra.setAttribute('aria-valuenow', datos.paginas_leidas);
    progresoBarra.setAttribute('aria-valuemax', window.numPaginas);
    console.log('stoy aqu1');
    actualizarTextoProgreso(response.paginas_leidas, response.num_pag, idLibro);// Llama a la función para actualizar el texto
    console.log('stoy aq2');
}

document.addEventListener("DOMContentLoaded", function() {
  const btnsActualizar = document.querySelectorAll('.btn-actualizar');
  btnsActualizar.forEach(btn => {
    btn.addEventListener('click', function(event) {
      event.preventDefault();
      const idLibro = this.dataset.id;
       console.log('tengo los id');
        console.log(idLibro);
     actualizarPaginasLeidas(idLibro); // Obtener el valor del atributo "data-id" del botón
    });
  });
});




function actualizarPaginasLeidas(idLibro) {
  var numPaginas = document.querySelector('.btn-actualizar[data-id="' + idLibro + '"]').dataset.paginas;
  var paginasLeidas = document.querySelector('#paginasLeidas' + idLibro).value;


console.log(idLibro);
  if (paginasLeidas < 0 || paginasLeidas > numPaginas) {
    alert('El número de páginas leídas debe estar entre 0 y ' + numPaginas + '.');
    return;
  }

  var data = { paginasLeidas };

  $.ajax({
    url: baseUrl+'/libros/actualizarpaginasleidas?id=' + idLibro,
    type: 'POST',
    data,
    success: function(response) {
      console.log(response.porcentaje_leido);
      if (response.success) {
        // Actualizar el valor del número de páginas leídas en la página
        $('#paginasLeidas' + idLibro).val(response.paginas_leidas);

        // Actualizar el ancho y el texto de la barra de progreso
        var porcentajeLeido = response.porcentaje_leido;
        var $progreso = $('#progresoLibro' + idLibro + ' .progress-bar');
        $progreso.css('width', porcentajeLeido + '%');
        $progreso.attr('aria-valuenow', response.paginas_leidas);
        //$progreso.html(response.paginas_leidas + ' / ' + response.num_pag + ' pág. (' + porcentajeLeido + '%)');
        actualizarTextoProgreso(response.paginas_leidas, response.num_pag, idLibro);

        // Actualizar el atributo paginas_leidas en el modelo libro
        $progreso.closest('.libro').attr('data-paginas_leidas', response.paginas_leidas);
      } else {
        alert('ERROR.');
      }
    },
    error: function() {
      alert('Ha ocurrido un error al actualizar las páginas leídas.');
    }
  });
}

