<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\FormRegister;
use app\models\Users;
use app\models\Libros;
use app\models\Agregar;
use app\models\Estanterias;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\web\JsonResponse;
use yii\web\JsExpression;
use yii\data\ArrayDataProvider;


class SiteController extends Controller
{
    
    
public function actionBuscar()
{
   
    $busqueda = Yii::$app->request->get('q');
    if (!$busqueda) {
        // Redirigir al usuario a la página de inicio o mostrar un mensaje de error
         return $this->goHome();
    }
$registros = Libros::find()
    ->joinWith('codEscritors')
    ->where(['like', 'titulo', $busqueda])
    ->orWhere(['like', 'escritores.nombre', $busqueda])
    ->all();

    
    return $this->render('resultado-busqueda', [
        'registros' => $registros,
        'busqueda' => $busqueda,
    ]);
}

// ...

public function actionBusquedaajax($q)
{
    $registros = Libros::find()
        ->joinWith('codEscritors')
        ->where(['like', 'titulo', $q])
        ->orWhere(['like', 'escritores.nombre', $q])
        ->all();

    return $this->asJson(['registros' => $registros]);
}



    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

     
    
    /**
     * Displays homepage.
     *
     * @return string
     */
public function actionIndex()
{
    // Obtener los datos de los libros para mostrarlos en la vista
    $dataProvider = new ActiveDataProvider([
        'query' => Libros::find(),
        'pagination' => [
            'pageSize' => 50, // número de elementos por página
    ],
    ]);

    // Obtener las estanterías para mostrarlas en la vista
     $estanterias = Estanterias::find()->all();
     $actual= Estanterias::find()->where('id=2')->all();

     //libros de lecturas actuales
     $estanteriaLeyendoActualmente = Estanterias::findOne(['nombre' => 'Leyendo actualmente']);
$estanteriaLeyendoActualmenteId = $estanteriaLeyendoActualmente->id;

$librosLeyendoActualmente = Libros::find()
    ->joinWith(['codEstanterias', 'codLector'])
    ->where(['estanterias.id' => $estanteriaLeyendoActualmenteId, 
        'users.id' => Yii::$app->user->identity->id])
    ->all();

    // Renderizar la vista y pasar los datos a la vista
    return $this->render('index', [
        'dataProvider' => $dataProvider,
        'estanterias' => $estanterias,
        'actual'=>$actual,
        'librosLeyendoActualmente'=>$librosLeyendoActualmente,
    ]);
}

public function actionEstanteria($id) {
    $estanteria = Estanterias::findOne($id);

    if ($estanteria !== null) {
        $this->view->title = 'Estanteria: ' . $estanteria->nombre;

        $libros = Libros::find()
            ->joinWith(['codEstanterias', 'codLector'])
            ->where([
                'estanterias.id' => $id,
                'users.id' => Yii::$app->user->identity->id
            ])
            ->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $libros,
        ]);

        return $this->render('estanteria', [
            'dataProvider' => $dataProvider,
            'estanteria'=>$estanteria,
        ]);
    } else {
        throw new NotFoundHttpException('La página que buscas no existe.');
    }
}

public function actionEstanteriatotal() {
    $estanteria = Estanterias::find()->all();

    if ($estanteria !== null) {
         $this->view->title = 'Todos mis libros';

        $libros = Libros::find()
            ->joinWith(['codEstanterias', 'codLector'])
            ->where([
                'users.id' => Yii::$app->user->identity->id
            ])
            ->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $libros,
        ]);

        return $this->render('estanteria', [
            'dataProvider' => $dataProvider,
            'estanteria'=>$estanteria,
        ]);
    } else {
        throw new NotFoundHttpException('La página que buscas no existe.');
    }
}

    
 
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }
    
    

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    

 
  public function randKey($str='', $long=0)
    {
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;
        for($x=0; $x<$long; $x++)
        {
            $key .= $str[rand($start, $limit)];
        }
        return $key;
    }
  

 
 public function actionRegister()
 {
  //Creamos la instancia con el model de validación
  $model = new FormRegister;
   
  //Mostrará un mensaje en la vista cuando el usuario se haya registrado
  $msg = null;
   
  //Validación mediante ajax
  if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
   
  //Validación cuando el formulario es enviado vía post
  //Esto sucede cuando la validación ajax se ha llevado a cabo correctamente
  //También previene por si el usuario tiene desactivado javascript y la
  //validación mediante ajax no puede ser llevada a cabo
  if ($model->load(Yii::$app->request->post()))
  {
   if($model->validate())
   {
    //Preparamos la consulta para guardar el usuario
    $table = new Users;
    $table->username = $model->username;
    $table->email = $model->email;
    $table->f_registro = date("Y-m-d");
    $table->biografia = $model->biografia;
    //Encriptamos el password
    $table->password = crypt($model->password, Yii::$app->params["salt"]);
    //Creamos una cookie para autenticar al usuario cuando decida recordar la sesión, esta misma
    //clave será utilizada para activar el usuario
    $table->authKey = $this->randKey("abcdef0123456789", 20);
    //Creamos un token de acceso único para el usuario
    $table->accessToken = $this->randKey("abcdef0123456789", 20);
     
    //Si el registro es guardado correctamente
    if ($table->insert())
    {     
     $model->username = null;
     $model->email = null;
     $model->password = null;
     $model->password_repeat = null;
     
     $msg = "Enhorabuena";
    }
    else
    {
     $msg = "Ha ocurrido un error al llevar a cabo tu registro";
    }
     
   }
   else
   {
    $model->getErrors();
   }
  }
  return $this->render('register', ['model' => $model]);
 }

 
 
 
}
